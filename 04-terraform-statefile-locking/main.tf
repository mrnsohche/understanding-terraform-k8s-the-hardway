## do not destroy project until after session ##

resource "aws_s3_bucket" "this" {
  count = length(var.bucket_name)

  bucket = var.bucket_name[count.index]

  lifecycle {
    prevent_destroy = true
  }

}

# resource "aws_s3_bucket_acl" "this" {
#   bucket = aws_s3_bucket.b.id
#   acl    = "private"
# }

resource "aws_dynamodb_table" "dynamodb-terraform-lock" {
  name           = "terraform-lock"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }
  lifecycle {
    prevent_destroy = true
  }
}