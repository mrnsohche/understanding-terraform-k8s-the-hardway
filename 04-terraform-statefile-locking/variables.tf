
variable "bucket_name" {
  type        = list(any)
  description = "The name of the state bucket"
  default     = ["04.terraform.state.bucket", "05.terraform.workspace.state"]
}