#################################
# Creating  VPC
#################################

resource "aws_vpc" "this" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "Kojitechs-vpc-${terraform.workspace}"
  }
}

#################################
# Creating  Internet Gatway
#################################

resource "aws_internet_gateway" "gw" {
  vpc_id = local.vpc_id

  tags = {
    Name = "Kojitechs-gw-${terraform.workspace}"
  }
}

#################################
# Creating  Public Subnets
#################################

resource "aws_subnet" "public_subnet" {
  count                   = length(var.public_subnet_cidr)
  vpc_id                  = local.vpc_id
  cidr_block              = var.public_subnet_cidr[count.index]
  availability_zone       = local.az[count.index] # ? use element
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-${count.index + 1}-${terraform.workspace}"
  }
}

# resource "aws_subnet" "public_subnet_2" {
#   vpc_id                  = local.vpc_id
#   cidr_block              = var.public_subnet_2_cidr
#   availability_zone       = "us-east-1b"
#   map_public_ip_on_launch = true

#   tags = {
#     Name = "public-subnet-2${terraform.workspace}"
#   }
# }

#################################
# Creating  Private Subnets
#################################
resource "aws_subnet" "private_subnet" {
  count = length(var.private_subnet_cidr)

  vpc_id            = local.vpc_id
  cidr_block        = var.private_subnet_cidr[count.index]
  availability_zone = local.az[count.index]

  tags = {
    Name = "private-subnet-${count.index + 1}-${terraform.workspace}"
  }
}

# resource "aws_subnet" "private_subnet_2" {
#   vpc_id            = local.vpc_id
#   cidr_block        = var.private_subnet_2_cidr
#   availability_zone = "us-east-1b"

#   tags = {
#     Name = "private-subnet-2${terraform.workspace}"
#   }
# }

#################################
# Creating  Data Subnets
#################################

# resource "aws_subnet" "database_subnet" {
#   for_each = var.database_subnet_cidr

#   vpc_id            = local.vpc_id
#   cidr_block        = each.value.cidr_block
#   availability_zone = each.value.az

#   tags = {
#     Name = "${each.key}-${terraform.workspace}"
#   }
# }


resource "aws_subnet" "database_subnet" {
  for_each = local.database_subnet

  vpc_id            = local.vpc_id
  cidr_block        = each.value.cidr_block
  availability_zone = each.value.az

  tags = {
    Name = "${each.key}-${terraform.workspace}"
  }
}

# resource "aws_subnet" "database_subnet_2" {
#   vpc_id            = local.vpc_id
#   cidr_block        = var.database_subnet_2_cidr
#   availability_zone = "us-east-1a"

#   tags = {
#     Name = "database-subnet-2${terraform.workspace}"
#   }
# }

##################################################
# Creating Public Route Table with Public Subnet #
##################################################

resource "aws_route_table" "public_route_table" {
  vpc_id = local.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }


  tags = {
    Name = "public-route-table"
  }
}

##############################################################
# Creating Public Route Tables Association with Public Subnet #
##############################################################


resource "aws_route_table_association" "rt_association" {
  count = length(var.public_subnet_cidr)

  subnet_id      = aws_subnet.public_subnet.*.id[count.index]
  route_table_id = aws_route_table.public_route_table.id
}

# resource "aws_route_table_association" "public_subnet_2" {
#   subnet_id      = aws_subnet.public_subnet_2.id
#   route_table_id = aws_route_table.public_route_table.id
# }

#################################
# Creating Default Route Tables #
#################################

resource "aws_default_route_table" "this" {
  default_route_table_id = aws_vpc.this.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.this.id
  }

  tags = {
    Name = "this"
  }
}


################################
#    Creating NAT Gateway      #
################################

resource "aws_nat_gateway" "this" {
  depends_on = [aws_internet_gateway.gw]

  allocation_id = aws_eip.eip.id
  #subnet_id     = aws_subnet.public_subnet[0].id
  subnet_id = aws_subnet.database_subnet["db_1"].id

  tags = {
    Name = "gw-NAT"
  }

}


################################
#    Creating An Elastic IP    #
################################

resource "aws_eip" "eip" {
  depends_on = [aws_internet_gateway.gw]
  vpc        = true
}