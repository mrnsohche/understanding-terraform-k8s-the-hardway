
data "aws_ami" "ami" {
  most_recent = true
  owners      = ["137112412989"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

#################################
# Creating Public EC2 Instance
#################################

resource "aws_instance" "app1" {
  ami                  = data.aws_ami.ami.id
  instance_type        = var.public_instance_type #prod-t3.xlarge : dev-t2.micro
  subnet_id            = aws_subnet.private_subnet[1].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  security_groups      = [aws_security_group.static_sg.id]        # listen to 80 from alb
  user_data            = file("${path.module}/templates/app1.sh") #bootstrapping with user data using the file function


  #   subnet_id       = aws_subnet.public_subnet[0].id
  #   key_name        = "kojitechs-demo-key"

  tags = {
    Name = "app1-${terraform.workspace}"
  }
}


#################################
# Creating Private EC2 Instance
#################################

resource "aws_instance" "app2" {
  ami                  = data.aws_ami.ami.id
  instance_type        = var.private_instance_type
  subnet_id            = aws_subnet.private_subnet[1].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  security_groups      = [aws_security_group.static_sg.id] # listen to 80 from alb
  user_data            = file("./templates/app2.sh")       # bootstrapping with user data using the file function 
  #   key_name        = "kojitechs-demo-key"


  tags = {
    Name = "app2-${terraform.workspace}"
  }
}


