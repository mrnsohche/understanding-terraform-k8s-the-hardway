
locals {
  vpc_id = aws_vpc.this.id
  az     = data.aws_availability_zones.available.names
  database_subnet = {
    db_1 = {
      cidr_block = "10.0.5.0/24"
      az         = local.az[0]
    }

    db_2 = {
      cidr_block = "10.0.7.0/24"
      az         = local.az[1]
    }
  }
}





# locals {
#   az = data.aws_availability_zones.available.names[1]
# }

# locals {
#   vpc_id = aws_vpc.kojitechs_vpc.id
# }

# # locals {
# #   public_subnet = {
# #     pub_subnet_1 = {
# #       cidr_block = "10.0.0.0/24"
# #       az         = "us-east-1a"
# #     }
# #     pub_subnet_2 = {
# #       cidr_block = "10.0.2.0/24"
# #       az         = "us-east-1b"
# #     }
# #   }
# #   # private_subnet = {}
# # }