
variable "vpc_cidr" {
  type        = string
  description = "Vpc network cidr address"
  # default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  type        = list(any)
  description = "List for public subnet_cidr"
}

# variable "public_subnet_2_cidr" {
#   type        = string
#   description = "value for public subnet_2_cidr"
#   #default = "10.0.2.0/24"
# }

variable "private_subnet_cidr" {
  type        = list(any)
  description = "List for private subnet_cidr"
  #default = "10.0.1.0/24"
}

# variable "private_subnet_2_cidr" {
#   type        = string
#   description = "value for private subnet_2_cidr"
#   #default = "10.0.3.0/24"
# }

variable "database_subnet_cidr" {
  type        = map(any)
  description = "List for database_subnet_cidr"
  #default = "10.0.5.0/24"
}

# variable "database_subnet_cidr" {
#   type        = map(any)
#   description = "List for database_subnet_cidr"
#   #default = "10.0.5.0/24"
# }

# variable "public_subnet" {
#   type        = map(any)
#   description = "objects of public_subnets to be created"
#   default = {
#     public_subnet_1 = {
#       cidr_block = "10.0.0.0/24"
#       az         = "us-east-1a"
#     }
#     public_subnet_2 = {
#       cidr_block = "10.0.2.0/24"
#       az         = "us-east-1b"
#     }
#   }
# }

# variable "database_subnet_2_cidr" {
#   type        = string
#   description = "value for private subnet_2_cidr"
#   #default = "10.0.7.0/24"
# }

variable "public_instance_type" {
  type        = string
  description = "Public instance type"
  default     = "t2.micro"
}

variable "private_instance_type" {
  type        = string
  description = "Private instance type"
  default     = "t2.micro"
}