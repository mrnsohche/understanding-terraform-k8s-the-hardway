#############################################
# Creating Security Group For Bastion Server 
#############################################

resource "aws_security_group" "alb_sg" {
  name        = "alb-sg-${terraform.workspace}"
  description = "Allow traffic on port 80"
  vpc_id      = local.vpc_id

  ingress {
    description = "Allow traffic on port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow traffic on port 80"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb-sg-${terraform.workspace}"
  }
}

###############################################
# Creating Security Group For Private Instance 
###############################################

resource "aws_security_group" "static_sg" {
  name        = "static-sg-${terraform.workspace}"
  description = "Allow inbound traffic from alb security group id"
  vpc_id      = local.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "static-sg-${terraform.workspace}"
  }
}

###############################################
# Creating Private Security Group Rule 
###############################################

resource "aws_security_group_rule" "allow_traffic_from_alb_sg" {
  security_group_id        = aws_security_group.static_sg.id
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.alb_sg.id # reference security group id of alb

}