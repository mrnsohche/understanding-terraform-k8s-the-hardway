#########################################################
# APPLICATION LOAD BALANCER 
#########################################################

resource "aws_lb" "this" {
  name               = "web-alb-${terraform.workspace}"
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = aws_subnet.public_subnet.*.id # == [for i in aws_subnet.public_subnet: i.id]

  tags = {
    Name = "web-alb-${terraform.workspace}"
  }
}

#########################################################
# TARGET GROUPS 
#########################################################

resource "aws_lb_target_group" "app1" {
  name        = "app1-tg-${terraform.workspace}"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = local.vpc_id
  target_type = "instance"

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 3
    interval            = 30
    protocol            = "HTTP"
    path                = "/app1/index.html"
    matcher             = "200-399"
    port                = "traffic-port"
  }

}

resource "aws_lb_target_group" "app2" {
  name        = "app2-tg-${terraform.workspace}"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = local.vpc_id
  target_type = "instance"

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 3
    interval            = 30
    protocol            = "HTTP"
    path                = "/app2/index.html"
    matcher             = "200-399"
    port                = "traffic-port"
  }

}

#########################################################
# Target Group attachments 
#########################################################

resource "aws_lb_target_group_attachment" "app1" {
  target_group_arn = aws_lb_target_group.app1.arn
  target_id        = aws_instance.app1.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "app2" {
  target_group_arn = aws_lb_target_group.app2.arn
  target_id        = aws_instance.app2.id
  port             = 80
}

#########################################################
# Load Balancer Listerners 
#########################################################

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.this.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

# resource "aws_lb_listener" "https" {
#   load_balancer_arn = aws_lb.this.arn
#   port              = 443
#   protocol          = "HTTPS"
#   ssl_policy        = "ELBSecurityPolicy-2016-08"
#   certificate_arn   = aws_acm_certificate.this.arn

#   default_action {
#     type = "fixed-response"

#     fixed_response {
#       content_type = "text/plain"
#       message_body = "Fixed response content"
#       status_code  = "200"
#     }
#   }
# }

#########################################################
# Load Balancer Listerners Rules 
#########################################################

resource "aws_lb_listener_rule" "app1" {
  listener_arn = aws_lb_listener.https.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app1.arn
  }

  condition {
    path_pattern {
      values = [
        "/app1*"
      ]
    }
  }
}

resource "aws_lb_listener_rule" "app2" {
  listener_arn = aws_lb_listener.https.arn
  priority     = 2

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app2.arn
  }

  condition {
    path_pattern {
      values = [
        "/app2*"
      ]
    }
  }
}

#########################################################
#
#########################################################