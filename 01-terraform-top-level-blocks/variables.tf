variable "subnet_1_cidr" {
  type = string
  description = "value for sbbnet_1_cidr"
  default = "10.0.1.0/24"
}

variable "subnet_2_cidr" {
  type = string
  description = "value for sbbnet_2_cidr"
  default = "10.0.2.0/24"
}

variable "kojitechs_vpc_cidr" {
  type = string
  description = "value for kojitechs_vpc_cidr"
  default = "10.0.0.0/16"
}

# data sources are better use case to make a code more reusable than variables

# variable "subnet_1_az" {
#   type = string
#   description = "value for subnet_1_az"
#   default = "us-east-1a"

# }

# variable "subnet_2_az" {
#   type = string
#   description = "value for subnet_2_az"
#   default = "us-east-1b"

# }