# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}

### in situations where you want to apply just a single resource we 
### make use of the cmd  (terraform apply -target data.aws_ami.ami)

data "aws_ami" "kojitechs_ami" {
 # executable_users = ["self"]
  most_recent      = true
  # name_regex       = "^myami-\\d{3}"
  owners           = ["137112412989"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
# data.aws_availability_zones.available.names