
output "public_ip" {
    description = "Fetch the public ip"
    value = aws_instance.web.public_ip
}

output "kojitechs_ami" {
  value = data.aws_ami.kojitechs_ami.id
}

output "kojitechs_ami2" {
  value = data.aws_ami.kojitechs_ami
}