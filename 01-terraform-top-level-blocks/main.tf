# Create a VPC
resource "aws_vpc" "kojitechs_vpc" {
  cidr_block = var.kojitechs_vpc_cidr

  tags = {
  Name = "kojitechs-vpc"
  }

}

# resouce_name.local_name.desired_attribute
## aws_subnet.  subnet_1.  id

## DATA SOURCE
resource "aws_subnet" "subnet_1" {
  vpc_id     = local.vpc_id
  cidr_block = var.subnet_1_cidr
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "subnet_1"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id     = local.vpc_id
  cidr_block = var.subnet_2_cidr
  availability_zone = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "subnet_2"
  }
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.kojitechs_ami.id
  instance_type = "t3.micro"
  #subnet_id =

  tags = {
    Name = "web"
  }
}

