
provider "aws" {
  region = "us-east-1"
  # // ONE AWS ACCOUNT (dev, sbx, prod)
}


# #this is use for terraform enterprise setup or for use of multiple aws accounts
# provider "aws" {
#   region = "us-east-1"

#   assume_role {
#     role_arn = "arn"
#   }

# }