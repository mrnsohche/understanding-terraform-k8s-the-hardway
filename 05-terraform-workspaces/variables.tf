variable "vpc_cidr" {
  type        = string
  description = "Vpc CIDR"

}

variable "subnet_1_cidr" {
  type        = string
  description = "Subnet 1 CIDR"

}

variable "subnet_2_cidr" {
  type        = string
  description = "Subnet 2 CIDR"

}

variable "instance_type" {
  type        = string
  description = "Instance Type"

}