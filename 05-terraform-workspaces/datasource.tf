

# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_ami" "ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"] #amzn2-ami-kernel-5.10-hvm-2.0.20230221.0-x86_64-gp2
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

## pulling down keypair using datasource

# data "aws_key_pair" "my_key_pair" {
#   key_name           = var.key_name # "kojictechs-demo-key" 
#   include_public_key = true

#   #   filter {
#   #     name   = "tag:name"
#   #     values = ["koji-keys"]
#   #   }
# }