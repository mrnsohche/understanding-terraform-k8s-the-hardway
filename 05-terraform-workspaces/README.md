## TERRAFORM PLAN IN WORKSPACES
\\
terraform plan -var-file sbx.tfvars
\\

## TERRAFORM apply IN WORKSPACES
\\
terraform apply -var-file sbx.tfvars
\\

