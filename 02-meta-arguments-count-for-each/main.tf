# ######################################
# #   COUNT FOR META -- ARGUMENTS      #
# ######################################

# # Create a VPC
# resource "aws_vpc" "kojitechs_vpc" {
#   cidr_block = var.kojitechs_vpc_cidr

#   tags = {
#   Name = "kojitechs-vpc"
#   }

# }

# # resouce_name.local_name.desired_attribute
# ## aws_subnet.  subnet_1.  id

# ## DATA SOURCE
# resource "aws_subnet" "public_subnets" {
#   kojitechs = length(var.public_subnet_1_cidr)
#   vpc_id     = local.vpc_id
#   cidr_block = var.public_subnet_1_cidr[kojitechs.index]
#   availability_zone = data.aws_availability_zones.available.names[kojitechs.index]
#   map_public_ip_on_launch = true

#   tags = {
#     Name = "public-subnets-${kojitechs.index + 1}" #concatination
#   }
# }

# resource "aws_subnet" "private_subnets" {
#   kojitechs = length(var.private_subnet_2_cidr)
#   vpc_id     = local.vpc_id
#   cidr_block = var.private_subnet_2_cidr[kojitechs.index]
#   availability_zone = data.aws_availability_zones.available.names[kojitechs.index]

#   tags = {
#     Name = "private-subnets-${kojitechs.index + 1}"
#   }
# }


# # resource "aws_subnet" "subnet_2_cidr" {
# #   kojitechs = 3
# #   vpc_id     = local.vpc_id
# #   cidr_block = var.subnet_2_cidr
# #   availability_zone = data.aws_availability_zones.available.names[1]

# #   tags = {
# #     Name = "subnet_2_cidr"
# #   }
# # }

# resource "aws_instance" "web" {
#   ami           = data.aws_ami.kojitechs_ami.id
#   instance_type = "t3.micro"
#   subnet_id = aws_subnet.public_subnets[0].id
#   #subnet_id =

#   tags = {
#     Name = "web"
#   }
# }

