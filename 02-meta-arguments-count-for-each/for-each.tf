######################################
#   For_each FOR META -- ARGUMENTS   #
######################################

# Create a VPC
resource "aws_vpc" "for_each_vpc" {
  cidr_block = var.kojitechs_vpc_cidr

  tags = {
    Name = "for_each-vpc"
  }

}

# resouce_name.local_name.desired_attribute
## aws_subnet.  subnet_1.  id

## DATA SOURCE
resource "aws_subnet" "public_subnets_for_each" {
  # count = length(var.public_subnet_1_cidr)

  # for_each = local.public_subnet
  for_each = var.public_subnet

  vpc_id                  = local.vpc_id
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.az
  map_public_ip_on_launch = true

  #   tags = {
  #     Name = "public-subnets-${count.index + 1}" #concatination
  #   }
  tags = {
    Name = each.key
  }
}

resource "aws_subnet" "private_subnets_for_each" {
  count             = length(var.private_subnet_2_cidr)
  vpc_id            = local.vpc_id
  cidr_block        = var.private_subnet_2_cidr[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = "private-subnets-${count.index + 1}"
  }
}


# resource "aws_subnet" "subnet_2_cidr" {
#   count = 3
#   vpc_id     = local.vpc_id
#   cidr_block = var.subnet_2_cidr
#   availability_zone = data.aws_availability_zones.available.names[1]

#   tags = {
#     Name = "subnet_2_cidr"
#   }
# }

# resource "aws_instance" "web_for_each" {
#   ami           = data.aws_ami.kojitechs_ami.id
#   instance_type = "t3.micro"
#   subnet_id     = aws_subnet.public_subnets_for_each[0].id
#   #subnet_id =

#   tags = {
#     Name = "web_for_each"
#   }
# }

