# ###############################
# #  ROOT MODULES 
# ###########################3###

# ## PRIVATE MODULES / PUBLIC MODULES


# module "vpc" {
#   source = "terraform-aws-modules/vpc/aws"

#   name = "module-vpc"
#   cidr = "10.0.0.0/16"

#   azs             = slice(var.module_az, 1, 3)
#   private_subnets = slice(var.module_private_subnet, 0, 3)
#   public_subnets  = slice(var.module_public_subnet, 0, 3)

#   enable_nat_gateway = var.nat_gateway
#   enable_vpn_gateway = var.vpn_gateway

#   tags = {
#     Terraform   = var.tags_module
#     Environment = var.tags_environment
#   }
# }