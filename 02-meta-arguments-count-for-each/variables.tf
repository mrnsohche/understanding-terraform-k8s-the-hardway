variable "public_subnet_1_cidr" {
  type        = list(any)
  description = "value for public_subnet_1_cidr"
  default     = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24", "10.0.6.0/24"]
}

variable "private_subnet_2_cidr" {
  type        = list(any)
  description = "value for private_subnet_2_cidr"
  default     = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]
}

variable "kojitechs_vpc_cidr" {
  type        = string
  description = "value for kojitechs_vpc_cidr"
  default     = "10.0.0.0/16"
}

# data sources are better use case to make a code more reusable than variables

# variable "subnet_1_az" {
#   type = string
#   description = "value for subnet_1_az"
#   default = "us-east-1a"

# }

# variable "subnet_2_az" {
#   type = string
#   description = "value for subnet_2_az"
#   default = "us-east-1b"

# }

## for each variable block

variable "public_subnet" {
  type        = map(any)
  description = "objects of public_subnets to be created"
  default = {
    public_subnet_1 = {
      cidr_block = "10.0.0.0/24"
      az         = "us-east-1a"
    }
    public_subnet_2 = {
      cidr_block = "10.0.2.0/24"
      az         = "us-east-1b"
    }
  }
}

### variables for modules

# variable "module_az" {
#   type        = list(string)
#   description = "az for module block"
#   default     = ["us-east-1a", "us-east-1b", "us-east-1c"]

# }

# variable "module_public_subnet" {
#   type        = list(string)
#   description = "pub_sub for module block"
#   default     = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]

# }

# variable "module_private_subnet" {
#   type        = list(string)
#   description = "priv_sub for module block"
#   default     = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]

# }

# variable "nat_gateway" {
#   type        = bool
#   description = "module nat_gateway"
#   default     = true
# }

# variable "vpn_gateway" {
#   type        = bool
#   description = "module vpn_gateway"
#   default     = true
# }

# variable "tags_module" {
#   type        = string
#   description = "module tags"
#   default     = "true"

# }

# variable "tags_environment" {
#   type        = string
#   description = "module tags"
#   default     = "dev"

# }