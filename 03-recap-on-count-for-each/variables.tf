variable "key_name" {
  type        = string
  description = "kojitechs keypair"
  default     = "kojitechs-demo-key"
}

variable "subnet_1_cidr" {
  type        = string
  description = "value for sbbnet_1_cidr"
  default     = "10.0.1.0/24"
}

variable "subnet_2_cidr" {
  type        = string
  description = "value for sbbnet_2_cidr"
  default     = "10.0.2.0/24"
}

variable "kojitechs_vpc_cidr" {
  type        = string
  description = "value for kojitechs_vpc_cidr"
  default     = "10.0.0.0/16"
}