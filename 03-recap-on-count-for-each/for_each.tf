# resource "aws_instance" "for_each_this" {
#   for_each = var.for_each_this

#   ami           = data.aws_ami.ami.id # "ami-006dcf34c09e50022" avoid hardcoding by making use datasource
#   instance_type = "t3.micro"
#   key_name      = data.aws_key_pair.my_key_pair.key_name #"kojitech-demo-key" #using a data source
#   subnet_id = [aws_subnet.subnet_1.id,aws_subnet.subnet_2.id][count.index]
#   #subnet_id = element([aws_subnet.subnet_1.id,aws_subnet.subnet_2.id], count.index) #using the function element

#   tags = {
#     Name = "app-${count.index + 1}"
#   }
# }