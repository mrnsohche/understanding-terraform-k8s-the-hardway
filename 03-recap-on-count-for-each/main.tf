##############################################################
#Create 2 ec2  instances making use of count / for_each block#
##############################################################

# Create a VPC
resource "aws_vpc" "kojitechs_vpc" {
  cidr_block = var.kojitechs_vpc_cidr

  tags = {
    Name = "kojitechs-vpc"
  }

}

# resouce_name.local_name.desired_attribute
## aws_subnet.  subnet_1.  id

## DATA SOURCE
resource "aws_subnet" "subnet_1" {
  vpc_id            = local.vpc_id
  cidr_block        = var.subnet_1_cidr
  availability_zone = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "subnet_1"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id            = local.vpc_id
  cidr_block        = var.subnet_2_cidr
  availability_zone = data.aws_availability_zones.available.names[1]
  map_public_ip_on_launch = true
  tags = {
    Name = "subnet_2"
  }
}

resource "aws_instance" "this" {
  count         = 2
  ami           = data.aws_ami.ami.id # "ami-006dcf34c09e50022" avoid hardcoding by making use datasource
  instance_type = "t3.micro"
  key_name      = data.aws_key_pair.my_key_pair.key_name #"kojitech-demo-key" #using a data source
  subnet_id = [aws_subnet.subnet_1.id,aws_subnet.subnet_2.id][count.index]
  #subnet_id = element([aws_subnet.subnet_1.id,aws_subnet.subnet_2.id], count.index) #using the function element

  tags = {
    Name = "app-${count.index + 1}"
  }
}

